import React from 'react'
import Banner from '../components/Banner'

function Error() {
	return (
		<Banner isError ={true}/>
	)
}

export default Error