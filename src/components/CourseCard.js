// import Card from 'react-bootstrap/Card';
// import Button from 'react-bootstrap/Button';
//import { useState, useEffect} from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}) {

    //console.log(props)
    //console.log(typeof props)
    //console.log(courseProp)
    const { name, description, price, _id } = courseProp
    //console.log(courseProp)


    return (
         <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
                {/*<Card.Text>Enrollees: {count}</Card.Text>
                <Card.Text>Seats: {seats}</Card.Text>
                <Button variant="primary" onClick={enroll}>Enroll</Button>*/}
                <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
    )

}