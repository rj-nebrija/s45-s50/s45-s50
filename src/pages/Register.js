import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function Register (){

	const { user } = useContext(UserContext)
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNum, setMobileNum] = useState('')
	const navigate = useNavigate()

	//console.log(email);
	//console.log(password1);
	//console.log(password2);

	function registerUser(e) {

		e.preventDefault()

		fetch('http://localhost:4000/users/checkEmail', {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			body : JSON.stringify({
				email : email
			})
		}).then(res => res.json())
		.then(data => {
			if(data == true) {

				Swal.fire({
                        title: "Duplicate Email Found!",
                        icon: "error",
                        text: "Please provide a different email."
                  })   

			} else {

				fetch('http://localhost:4000/users/register', {
					method : 'POST',
					headers : {
						'Content-Type' : 'application/json'
					},
					body : JSON.stringify({
						firstName : firstName,
						lastName : lastName,
						email : email,
						mobileNo : mobileNum,
						password : password1
					})
				}).then(res => res.json())
				.then(data2 => {

					if(data2) {
						Swal.fire({
                        title: "Email Registered!",
                        icon: "success",
                        text: "Welcome to Zuitt!"
                  })


						setEmail('');
						setPassword1('');
						setPassword2('');
						setFirstName('')
						setLastName('')
						setMobileNum('')


						navigate('/login')
					} else {
						Swal.fire({
                        title: "Server Error",
                        icon: "error",
                        text: "please do it again!"
                  })  
					}

						
				})
			}
		})
	
	}

	useEffect(() => {

		if((email !== '' && password1 !== '' && password2 !== '' && firstName !== '' && lastName !== '' && mobileNum !== '') && (password1 === password2) && (mobileNum.length == 11)) {
			setIsActive(true);
		} else {
			setIsActive(false)
		}

	},  [email, password1, password2, firstName, lastName, mobileNum])


	console.log("USER")
	console.log(user)

	return(
		
		(user.id !== null) ? 

			<Navigate to="/courses"/>
			
			:

			<Form onSubmit = {(e) => registerUser(e)}>
			  <Form.Group className="mb-3" controlId="firstName">
			    <Form.Label>First Name</Form.Label>
			    <Form.Control 
			    				type="text" 
			    				placeholder="Enter First Name"
			    				value = {firstName}
			    				onChange = {e => setFirstName(e.target.value)}
			    				required />
			  </Form.Group>


			  <Form.Group className="mb-3" controlId="lastName">
			    <Form.Label>Last Name</Form.Label>
			    <Form.Control 
			    				type="text" 
			    				placeholder="Enter last name"
			    				value = {lastName}
			    				onChange = {e => setLastName(e.target.value)}
			    				required />
			  </Form.Group>


			  <Form.Group className="mb-3" controlId="userEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    				type="email" 
			    				placeholder="Enter email"
			    				value = {email}
			    				onChange = {e => setEmail(e.target.value)}
			    				required />
			    <Form.Text className="text-muted">
			      We'll never share your email with anyone else.
			    </Form.Text>
			  </Form.Group>

				<Form.Group className="mb-3" controlId="mobileNum">
			    <Form.Label>Mobile Number</Form.Label>
			    <Form.Control 
			    				type="text" 
			    				placeholder="0912345678"
			    				value = {mobileNum}
			    				onChange = {e => setMobileNum(e.target.value)}
			    				required />
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="password1">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
			    				type="password" 
			    				placeholder="Password"
			    				value={password1}
			    				onChange= {e => setPassword1(e.target.value)}
			    				required />
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="password2">
			    <Form.Label> Verify Password</Form.Label>
			    <Form.Control 
			    				type="password" 
			    				placeholder="Verify Password"
			    				value={password2}
			    				onChange={e=> setPassword2(e.target.value)}
			    				required />
			  </Form.Group>

			  {
			  	isActive ? 
			  		<Button variant="primary" type="submit" id="submitBtn">
			  		  Submit
			  		</Button>
			  		:
			  		<Button variant="danger" type="submit" id="submitBtn" disabled>
			  		  Submit
			  		</Button>
			  }

			</Form>
		
		)

}

/*

	Mini-Activity:
		If the user goes to /register route (Registration Page) when the user is already logged in, navigate/redirect to the /courses route (Courses Page)

		6:30 PM

*/